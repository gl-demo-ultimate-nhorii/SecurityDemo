<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
        SQLインジェクションに関する脆弱性を含んだコード
        $con = mysql_connect("localhost","sqli","sqli");
        if (!$con) {
          die('Could not connect: ' . mysql_error());
        }
        mysql_select_db("sqliexample", $con);
        $id = $_GET['id'];
        $stmt = $con->prepare("SELECT name FROM user WHERE id=?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();      
        mysql_close($con);
        $num = mysql_num_rows($result);
        $i=0;
        while ($i < $num) {
          $name = mysql_result($result, $i, "name");
          echo "Hello " . $name;
          echo "<br/>";
          $i++;
        }
    
        //上記の脆弱性をPrepared関数を利用するこどで解消
        // $con = mysql_connect("localhost", "sqli", "sqli");
        // if (!$con) {
        // die('Could not connect: ' . mysql_error());
        // }
        // mysql_select_db("sqliexample", $con);
        // $id = $_GET['id'];
    
        // // プリペアドステートメントを準備
        // $stmt = mysql_prepare($con, "SELECT name FROM user WHERE id=?");
        // if ($stmt === false) {
        // die('Could not prepare statement: ' . mysql_error());
        // }
    
        // // パラメーターをバインド
        // mysql_stmt_bind_param($stmt, "i", $id);
    
        // // クエリを実行
        // $result = mysql_stmt_execute($stmt);
        // if ($result === false) {
        // die('Could not execute statement: ' . mysql_error());
        // }
    
        // // 結果を取得
        // $num = mysql_stmt_num_rows($stmt);
        // $i = 0;
        // while ($i < $num) {
        // $name = mysql_stmt_result($stmt, $i, "name");
        // echo "Hello " . $name;
        // echo "<br/>";
        // $i++;
        // }
        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request -> has( 'keyword' )) {
            $items = Item::where('name', 'Like',  '%'.$request->get('keyword').'%') -> paginate(15);
        }
        else {
            $items = Item::paginate(15);
        }
        return view('item/index', ['items' => $items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return view( 'item/show' , ['item' =>$item]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }
}
